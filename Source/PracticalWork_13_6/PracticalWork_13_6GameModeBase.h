// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PracticalWork_13_6GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICALWORK_13_6_API APracticalWork_13_6GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
