// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"
#include "LODGenerationCommandlet.generated.h"

/**
 * 
 */
UCLASS()
class AUTOMATIONOFLODGENERATION_API UGenerateLODsCommandlet : public UCommandlet
{
	GENERATED_BODY()
	
	// The main function of the commandlet
	virtual int32 Main(const FString& Params) override;

	/** Processing assets in accordance with the transferred directories
	* @Param RootDirectories - String array of asset directories
	*/
	void ProcessAssets(TArray<FString> RootDirectories);

	/** Changing the LOD of the transferred asset
	* @Param AssetInstance - An instance of the asset class to change the LOD
	*/
	void ModifyLod(UObject* AssetInstance);

	/** Saving the state of the transferred asset
	* @Param AssetInstance - An instance of the asset class to save the state
	*/
	void SaveAsset(UObject* AssetInstance);
};
